import { useState } from "react";
import { MenuOutlined } from "@ant-design/icons";
import logoIcon from "../../assets/icons/logo.svg";
import "./styles.css";

import { Drawer, Button, Image } from "antd";

function AppHeader() {
  const [visible, setVisible] = useState<boolean>(false);

  const showDrawer = () => {
    setVisible(true);
  };

  const onClose = () => {
    setVisible(false);
  };

  return (
    <div className="home-navbar">
      <div className="header">
        <div className="logo">
          <Image src={logoIcon} preview={false} width={60} height={60} />
        </div>
        <div className="mobileHidden">
          <p className="navbar-item">Home</p>
          <p className="navbar-item navbar-item-selected">Products</p>
          <p className="navbar-item">Offers</p>
          <p className="navbar-item">Sign in</p>
          <p className="navbar-item navbar-item-filled">Become a member</p>
        </div>
        <div className="mobileVisible">
          <Button
            type="primary"
            onClick={showDrawer}
            className="mobile-menu-button"
          >
            <MenuOutlined />
          </Button>
          <Drawer
            placement="right"
            closable={false}
            onClose={onClose}
            open={visible}
          >
            <p className="navbar-item">Home</p>
            <p className="navbar-item navbar-item-selected">Products</p>
            <p className="navbar-item">Offers</p>
            <p className="navbar-item">Sign in</p>
            <p className="navbar-item">Become a member</p>
          </Drawer>
        </div>
      </div>
    </div>
  );
}

export default AppHeader;
