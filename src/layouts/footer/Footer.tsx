import { Col, Image, Row } from "antd";
import { FunctionComponent } from "react";
import footerBackground from "../../assets/backgrounds/footer.svg";
import footerLogo from "../../assets/icons/footer-logo.svg";
import facebookIcon from "../../assets/icons/facebook.svg";
import telegramIcon from "../../assets/icons/telegram.svg";
import linkedinIcon from "../../assets/icons/linkedin.svg";
import "./styles.css";

interface HomeFooterProps {}

const HomeFooter: FunctionComponent<HomeFooterProps> = () => {
  return (
    <div
      className="footer-container"
      style={{
        background: `url(${footerBackground})`,
      }}
    >
      <Row gutter={[16, 48]}>
        <Col
          lg={{ span: 6 }}
          md={{ span: 12 }}
          sm={{ span: 12 }}
          xs={{ span: 12 }}
        >
          <Image
            src={footerLogo}
            width={50}
            height={50}
            className="footer-logo"
          />
          <div className="footer-logo-text">
            lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolo.
          </div>
        </Col>
        <Col
          lg={{ span: 6 }}
          md={{ span: 12 }}
          sm={{ span: 12 }}
          xs={{ span: 12 }}
        >
          <div className="sub-menu-title">Quick access</div>

          <div className="sub-menu-text">Home</div>
          <div className="sub-menu-text">Product</div>
          <div className="sub-menu-text">Offers</div>
          <div className="sub-menu-text">About</div>
          <div className="sub-menu-text">Contact</div>
        </Col>
        <Col
          lg={{ span: 6 }}
          md={{ span: 12 }}
          sm={{ span: 12 }}
          xs={{ span: 12 }}
        >
          <div className="sub-menu-title">Community</div>
          <div className="sub-menu-text">Give your feedback</div>
          <div className="sub-menu-text">Explore</div>
          <div className="sub-menu-text">Ask a question</div>
        </Col>
        <Col
          lg={{ span: 6 }}
          md={{ span: 12 }}
          sm={{ span: 12 }}
          xs={{ span: 12 }}
        >
          <div className="sub-menu-title">Contacts</div>
          <div className="sub-menu-text">Toll free: +963 9321 221 211</div>
          <div className="sub-menu-text">{"(6:AM to 5:PM)"}</div>
          <div className="sub-menu-text">
            <b>Email:</b> sample@gmail.com
          </div>
          <br />
          <Row gutter={16}>
            <Col>
              <Image
                src={telegramIcon}
                width={20}
                height={20}
                preview={false}
                className="footer-icon"
              />
            </Col>
            <Col>
              <Image
                src={facebookIcon}
                width={20}
                height={20}
                preview={false}
                className="footer-icon"
              />
            </Col>
            <Col>
              <Image
                src={linkedinIcon}
                width={20}
                height={20}
                preview={false}
                className="footer-icon"
              />
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
};

export default HomeFooter;
