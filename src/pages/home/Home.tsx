import { FunctionComponent } from "react";
import BestOffer from "../../features/best-offer/BestOffer";
import ProductDetails from "../../features/product-details/ProductDetails";
import RelatedOffers from "../../features/related-offers/RelatedOffers";
import "./styles.css";

interface HomeProps {}

const Home: FunctionComponent<HomeProps> = () => {
  return (
    <div className="home-page-container">
      <ProductDetails />
      <RelatedOffers />
      <BestOffer />
    </div>
  );
};

export default Home;
