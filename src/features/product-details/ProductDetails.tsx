import { Button, Checkbox, Col, Row } from "antd";
import { FunctionComponent, useState } from "react";
import ImagesSlider from "../../components/images-slider/ImagesSlider";
import productDetailsImage from "../../assets/images/product-details.svg";
import offerImage from "../../assets/images/offer-image.png";
import "./styles.css";

interface ProductDetailsProps {}

const ProductDetails: FunctionComponent<ProductDetailsProps> = () => {
  const [quantity, setQuantity] = useState<number>(1);
  return (
    <div className="product-details-container">
      <Row gutter={16}>
        <Col span={10}>
          <ImagesSlider
            images={[
              productDetailsImage,
              offerImage,
              productDetailsImage,
              productDetailsImage,
            ]}
          />
        </Col>
        <Col span={14} className="details-container">
          <span className="discount-percentage-tag">15% OFF</span>
          <div className="category-breadcrumb">Category/ MEN’s shower soup</div>
          <Row>
            <Col span={18}>
              <div className="product-name">SUNSET FLEUR</div>
              <div className="product-description">
                Lorem Ipsum Is Placeholder Text Commonly Used In The Graphic,
                Print, And Publishing Industries Layout
              </div>
              <div className="product-details-title">Product details:</div>

              <div className="product-quantity">
                Quantity{"   "}
                <span
                  className="minus"
                  onClick={() =>
                    setQuantity(quantity > 0 ? quantity - 1 : quantity)
                  }
                >
                  -
                </span>
                {quantity}
                <span
                  onClick={() => setQuantity(quantity + 1)}
                  className="plus"
                >
                  +
                </span>
              </div>
              <br />
              <span>
                <b className="product-size">Size {"   "}</b>
              </span>
              <Checkbox.Group
                options={["Small", "Medium", "Large"]}
                defaultValue={["Apple"]}
                style={{
                  fontWeight: "bold",
                }}
              />
            </Col>
            <Col span={6} className="product-price">
              <div className="product-old-price">10,000 SYP</div>
              <div className="product-new-price">10,000 SYP</div>
              <Button size="large" className="add-to-cart-button">
                Add to cart
              </Button>
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
};

export default ProductDetails;
