import { Button, Col, Image, Row } from "antd";
import { FunctionComponent } from "react";
import footerBackground from "../../assets/backgrounds/footer.svg";
import offerImage from "../../assets/images/offer-image.png";
import "./styles.css";

interface BestOfferProps {}

const BestOffer: FunctionComponent<BestOfferProps> = () => {
  return (
    <div
      className="best-offer-container"
      style={{
        background: `url(${footerBackground})`,
        backgroundSize: "cover",
      }}
    >
      <Row>
        <Col xs={{ span: 24 }} md={{ span: 12 }}>
          <div className="best-offer-description-container">
            <div className="days-left-description">3 Days Left</div>
            <div className="offer-title-description">Best Offer</div>
            <div className="offer-title-description">As you need it.</div>
            <div className="offer-description">
              Lorem ipsum is placeholder text commonly used in the graphic,
              print, and publishing industries for previewing layouts and visual
              mockups.
            </div>

            <Button type="default" className="get-offer-button" size="large">
              Get the offer
            </Button>
          </div>
        </Col>
        <Col xs={{ span: 24 }} md={{ span: 12 }}>
          <div className="best-offer-image-container">
            <Image src={offerImage} preview={false} width="50%" height="90%" />
            <div className="best-offer-price">5000SYP</div>
            <div className="best-offer-old-price">5000SYP</div>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default BestOffer;
