import { Col, Row } from "antd";
import { FunctionComponent } from "react";
import OfferCard from "../../components/offer-card/OfferCard";
import "./styles.css";

interface RelatedOffersProps {}

const RelatedOffers: FunctionComponent<RelatedOffersProps> = () => {
  return (
    <div className="related-offers-container">
      <div className="related-offers-title">Related Offers</div>
      <div className="related-offers-sub-title">
        Lorem ipsum is placeholder text commonly used.
      </div>
      <Row gutter={[32, 32]}>
        <Col
          xs={{ span: 12 }}
          sm={{ span: 8 }}
          md={{ span: 6 }}
          lg={{ span: 6 }}
          xl={{ span: 4 }}
        >
          <OfferCard />
        </Col>
        <Col
          xs={{ span: 12 }}
          sm={{ span: 8 }}
          md={{ span: 6 }}
          lg={{ span: 6 }}
          xl={{ span: 4 }}
        >
          <OfferCard />
        </Col>
        <Col
          xs={{ span: 12 }}
          sm={{ span: 8 }}
          md={{ span: 6 }}
          lg={{ span: 6 }}
          xl={{ span: 4 }}
        >
          <OfferCard />
        </Col>
        <Col
          xs={{ span: 12 }}
          sm={{ span: 8 }}
          md={{ span: 6 }}
          lg={{ span: 6 }}
          xl={{ span: 4 }}
        >
          <OfferCard />
        </Col>
        <Col
          xs={{ span: 12 }}
          sm={{ span: 8 }}
          md={{ span: 6 }}
          lg={{ span: 6 }}
          xl={{ span: 4 }}
        >
          <OfferCard />
        </Col>
        <Col
          xs={{ span: 12 }}
          sm={{ span: 8 }}
          md={{ span: 6 }}
          lg={{ span: 6 }}
          xl={{ span: 4 }}
        >
          <OfferCard />
        </Col>
      </Row>
    </div>
  );
};

export default RelatedOffers;
