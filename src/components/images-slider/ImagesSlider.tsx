import { Col, Image, Row } from "antd";
import { FunctionComponent, useState } from "react";
import "./styles.css";

interface ImagesSliderProps {
  images: Array<string>;
}

const ImagesSlider: FunctionComponent<ImagesSliderProps> = ({ images }) => {
  const [selectedIndex, setSelectedIndex] = useState<number>(0);

  return (
    <div className="images-slider-container">
      <Image
        src={images[selectedIndex]}
        preview={false}
        width="100%"
        height="75vh"
        className="main-image"
      />
      <Row className="sub-images-container">
        {images.map((image: string, index: number) => (
          <Col span={6} key={`${image}${index}`}>
            <Image
              src={image}
              preview={false}
              className="sub-image"
              onClick={() => {
                setSelectedIndex(index);
              }}
            />
          </Col>
        ))}
      </Row>
    </div>
  );
};

export default ImagesSlider;
