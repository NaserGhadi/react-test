import { Col, Image, Row } from "antd";
import { FunctionComponent } from "react";
import offerImage from "../../assets/images/offer-image.png";
import addIcon from "../../assets/icons/add-icon.svg";
import "./styles.css";

interface OfferCardProps {}

const OfferCard: FunctionComponent<OfferCardProps> = () => {
  return (
    <div className="offer-card-container">
      <Row>
        <Col span={12} className="offer-card-header-container">
          <div className="days-left-tag">10 days left</div>
        </Col>
        <Col span={12} className="offer-card-header-container">
          <div className="offer-price">2000SYP</div>
          <div className="offer-old-price">2000SYP</div>
        </Col>
      </Row>
      <Row>
        <Col>
          <Image src={offerImage} preview={false} width="100%" height="100%" />
        </Col>
      </Row>
      <Row>
        <Col>
          <div className="offer-title">Pepsi package</div>
        </Col>
      </Row>
      <Row>
        <Col span={20}>
          <div className="offer-sub-title">Pepsi package</div>
          <div className="offer-description">{"20X Plastic Jar(1Kg)"}</div>
        </Col>
        <Col span={4} className="offer-add-icon-container">
          <Image src={addIcon} preview={false} width="75%" />
        </Col>
      </Row>
    </div>
  );
};

export default OfferCard;
