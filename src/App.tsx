import "./App.css";
import HomeFooter from "./layouts/footer/Footer";
import HomeNavbar from "./layouts/navbar/HomeNavbar";
import Home from "./pages/home/Home";

function App() {
  return (
    <div className="App">
      <HomeNavbar />
      <Home />
      <HomeFooter />
    </div>
  );
}

export default App;
